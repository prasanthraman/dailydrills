/*
NOTE: Do not change the name of this file

NOTE: For all file operations use promises with fs. Promisify the fs callbacks rather than use fs.promises.

Q1. Create 2 files simultaneously (without chaining).
Wait for 2 seconds and starts deleting them one after another. (in order)
(Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)


Q2. Create a new file with lipsum data (you can google and get this). 
Do File Read and write data to another file
Delete the original file 
Using promise chaining
*/



const fs = require("fs")
const util = require("util")
const path = require("path")


function problem1() {
    const unlinkFile = util.promisify(fs.unlink)
    const writeFile = util.promisify(fs.writeFile)

    let fileNames = ['file1.txt', 'file2.txt']
    let createPromises
    createPromises = fileNames.map((file) => {
        return createFile(path.join(__dirname, file), 'Sample')
    })
    setTimeout(() => {
        deleteFile(path.join(__dirname, fileNames[0]))
            .then((status) => {
                console.log(status)
                return deleteFile(path.join(__dirname, fileNames[1]))
            })
            .then((status) => {
                console.log(status)
            })
            .catch((err) => {
                console.log(err)
            })

    }, 2 * 1000)

    function deleteFile(fileName) {
        return new Promise((resolve, reject) => {
            unlinkFile(fileName, (err) => {
                if (err) {
                    reject(err)
                } else {
                    resolve(`deleted ${fileName}`)
                }
            })
        })

    }
    function createFile(fileName, data) {
        return new Promise((resolve, reject) => {
            writeFile(fileName, data, (err) => {
                if (err) {
                    reject(err)
                } else {
                    resolve("Success Writing:", fileName)
                }
            })
        })

    }

}

function problem2() {
    const readFile = util.promisify(fs.readFile)
    const writeFile = util.promisify(fs.writeFile)

    readLipsum(path.join(__dirname, `lipsum.txt`))
        .then((data) => {
            return createFile(path.join(__dirname, `newLipsum.txt`), data)
        })
        .then((status) => {
            console.log(status)
        })
        .catch((err) => {
            console.log(err)
        })

    function readLipsum(fileName) {
        return new Promise((resolve, reject) => {
            readFile(fileName, { encoding: 'utf-8' }, (err, data) => {
                if (err) {
                    reject(err)
                } else {
                    resolve(data)
                }
            })
        })
    }

    function createFile(fileName, data) {
        return new Promise((resolve, reject) => {
            writeFile(fileName, data, (err) => {
                if (err) {
                    reject(err)
                } else {
                    resolve(`Success Writing: ${fileName}`)
                }
            })
        })

    }

}

function problem3() {
    const appendFile = util.promisify(fs.appendFile)

    login("Prasanth", 2)
        .then((user) => {
            return logData(user, `Login Success`)
        })


    function login(user, val) {
        if (val % 2 === 0) {
            return Promise.resolve(user);
        } else {
            return Promise.reject(new Error("User not found"));
        }
    }

    function getData() {
        return Promise.resolve([
            {
                id: 1,
                name: "Test",
            },
            {
                id: 2,
                name: "Test 2",
            }
        ]);
    }

    function logData(user, activity) {
        // use promises and fs to save activity in some file
        return new Promise((resolve, reject) => {
            appendFile(path.join(__dirname, `activity.log`), `${user}:${activity}`, (err) => {
                if (err) {
                    reject(err)
                } else {
                    resolve("Activity Logged")
                }
            })
        })
    }

}

/*
Q3.
Use appropriate methods to 
A. login with value 3 and call getData once login is successful
B. Write logic to logData after each activity for each user. Following are the list of activities
    "Login Success"
    "Login Failure"
    "GetData Success"
    "GetData Failure"

    Call log data function after each activity to log that activity in the file.
    All logged activity must also include the timestamp for that activity.
    
    You may use any data you want as the `user` object as long as it represents a valid user (eg: For you, for your friend, etc)
    All calls must be chained unless mentioned otherwise.
    
*/

problem1()
problem2()
problem3()
const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interest: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pikey": {
        age: 22,
        desgination: "Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    },
    "Pikfe": {
        age: 44,
        desgination: "Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}


/*

Q1 Find all users who are interested in playing video games.
Q2 Find all users staying in Germany.
Q3 Sort users based on their seniority level 
   for Designation - Senior Developer > Developer > Intern
   for Age - 20 > 10
Q4 Find all users with masters Degree.
Q5 Group users based on their Programming language mentioned in their designation.

NOTE: Do not change the name of this file

*/
function gamers(data) {
    let result = Object.entries(data)
    result = result.filter((recordArray) => {
        if (recordArray[1].hasOwnProperty("interests")) {
            return (recordArray[1].interests[0].includes("Video Games"))
        } if (recordArray[1].hasOwnProperty("interest")) {
            return (recordArray[1].interest[0].includes("Video Games"))
        } else {
            return false
        }
    })
    result = Object.fromEntries(result)

    return result
}

function germans(data) {
    let result = Object.entries(data)
    result = result.filter(([, value]) => {
        return (value.nationality == "Germany")
    })
    result = Object.fromEntries(result)
    console.log(result)

    return result
}

function masters(data) {
    let result = Object.entries(data)
    result = result.filter(([, value]) => {
        return value.qualification.includes("Master")
    })
    result = Object.fromEntries(result)
    console.log(result)

    return result
}
function sortUsers(data) {
    let result = Object.entries(data)
    result = result.map(([key, value]) => {
        if (value.desgination.includes("Senior")) {
            value["power"] = 2
        }else if (value.desgination.includes("Intern")) {
            value["power"] = 0
        } else {
            value["power"] = 1
        }
        return [key, value]
    })

    result = result.sort(([, value], [, valueOne]) => {
        if(valueOne.power==value.power){
            return(valueOne.age-value.age)
        }
        return (valueOne.power) - (value.power)

        //return valueOne.power >value.power? 1: value.power>valueOne.power?-1: valueOne.age>value.age?1:value.age>valueOne.age?-1:0
    })
    result = Object.fromEntries(result)
    console.log(result)

    return result
}

function groupProgrammers(data){
    let ignore="Senior Developer Intern - "
    let result=Object.entries(data)
        result=result.reduce((acc,[key,value])=>{
            let designation= value.desgination.split(" ")
            let language=designation.find((word)=>{
                return !(ignore.includes(word))
            })
            if(typeof(language)==="undefined"){
                language="Others"
            }
            if(acc.hasOwnProperty(language)){
                acc[language].push(key)
            }else{
                acc[language]=new Array()
                acc[language].push(key)
            }
            return acc
        },{})

        console.log(result)
    return result
}
//gamers(users)
//germans(users)
//masters(users)
sortUsers(users)
//groupProgrammers(users)
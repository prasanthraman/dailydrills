let data = [{ "id": 1, "first_name": "Valera", "last_name": "Pinsent", "email": "vpinsent0@google.co.jp", "gender": "Male", "ip_address": "253.171.63.171" },
{ "id": 2, "first_name": "Kenneth", "last_name": "Hinemoor", "email": "khinemoor1@yellowbook.com", "gender": "Polygender", "ip_address": "50.231.58.150" },
{ "id": 3, "first_name": "Roman", "last_name": "Sedcole", "email": "rsedcole2@addtoany.com", "gender": "Genderqueer", "ip_address": "236.52.184.83" },
{ "id": 4, "first_name": "Lind", "last_name": "Ladyman", "email": "lladyman3@wordpress.org", "gender": "Male", "ip_address": "118.12.213.144" },
{ "id": 5, "first_name": "Jocelyne", "last_name": "Casse", "email": "jcasse4@ehow.com", "gender": "Agender", "ip_address": "176.202.254.113" },
{ "id": 6, "first_name": "Stafford", "last_name": "Dandy", "email": "sdandy5@exblog.jp", "gender": "Female", "ip_address": "111.139.161.143" },
{ "id": 7, "first_name": "Jeramey", "last_name": "Sweetsur", "email": "jsweetsur6@youtube.com", "gender": "Genderqueer", "ip_address": "196.247.246.106" },
{ "id": 8, "first_name": "Anna-diane", "last_name": "Wingar", "email": "awingar7@auda.org.au", "gender": "Agender", "ip_address": "148.229.65.98" },
{ "id": 9, "first_name": "Cherianne", "last_name": "Rantoul", "email": "crantoul8@craigslist.org", "gender": "Genderfluid", "ip_address": "141.40.134.234" },
{ "id": 10, "first_name": "Nico", "last_name": "Dunstall", "email": "ndunstall9@technorati.com", "gender": "Female", "ip_address": "37.12.213.144" }]

/* 

    1. Find all people who are Agender
    2. Split their IP address into their components eg. 111.139.161.143 has components [111, 139, 161, 143]
    3. Find the sum of all the second components of the ip addresses.
    3. Find the sum of all the fourth components of the ip addresses.
    4. Compute the full name of each person and store it in a new key (full_name or something) for each person.
    5. Filter out all the .org emails
    6. Calculate how many .org, .au, .com emails are there
    7. Sort the data in descending order of first name

    NOTE: Do not change the name of this file

*/

function Agender(data) {
    let Agender = []

    Agender = data.filter(callBackForAgender)

    //console.log(Agender)
    return Agender

    function callBackForAgender(record) {
        let gender = record.gender
        if (gender.includes("Agender")) {
            return true
        }
        else {
            return false
        }
    }
}

function splitComponents(data) {

    let ipAddress = data.map(returnOnlyIP)
        .map(split)
    console.log(ipAddress)
    return (ipAddress)

    function returnOnlyIP(record) {
        return record.ip_address
    }

    function split(str) {
        if (typeof (str) != 'string') {
            return 0
        }
        let regEx = /[^.\d]/ //any character that is not a dot or digit.
        if (str.match(regEx)) {
            return [];
        }
        let components = str.split('.');
        for (let each in components) {
            if ((parseInt(components[each]) > 255) || parseInt(components[each] < 0) || components.length > 4) {
                //Invalid IP
                return [];
            }
            else {
                components[each] = parseInt(components[each])
            }
        }

        return components
    }
}

function sumOfSecondComponents(IpComponents) {
    let secondComponents = IpComponents.map(returnOnlySecond)
        .reduce((sum = 0, current_ip) => sum + current_ip)

    return (secondComponents)

    function returnOnlySecond(ipArr) {
        return ipArr[1]
    }
}
function sumOfFourthComponents(IpComponents) {
    let fourthComponents = IpComponents.map(returnOnlySecond)
        .reduce((sum = 0, current_ip) => sum + current_ip)
    console.log(fourthComponents)
    return (fourthComponents)

    function returnOnlySecond(ipArr) {
        return ipArr[3]
    }
}

function computeFullName(data) {
    fullNames = data.map(appendFullName)
    //console.log(fullNames)
    return fullNames

    function appendFullName(record) {
        record['full_name'] = record.first_name + " " + record.last_name
        return record
    }
}

function filterOrg(data) {
    let result = data.filter(returnOrg)
    //console.log(result)
    return result

    function returnOrg(record) {
        if (record.email.endsWith(".org")) {
            return true
        } else {
            return false
        }
    }
}

function countEmails(data) {
    let result = data.filter(returnOrgComAu)
        .length
    console.log(result)
    return result

    function returnOrgComAu(record) {
        if (record.email.endsWith(".org") || record.email.endsWith(".com") || record.email.endsWith(".au")) {
            return true
        } else {
            return false
        }
    }
}

function firstName(data) {
    let result = data.sort(returnSorted)

    console.log(result)
    return result

    function returnSorted(record, recordOne) {
        let name = record.first_name.toLowerCase()
        let nameOne = recordOne.first_name.toLowerCase()
        
        return name < nameOne ? 1 : name > nameOne ? -1 : 0;
    }
}

//Calling the function
Agender(data)
let IpComponents = splitComponents(data)
sumOfSecondComponents(IpComponents)
sumOfFourthComponents(IpComponents)
computeFullName(data)
filterOrg(data)
countEmails(data)
firstName(data)

const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];


/*

    1. Get all items that are available 
    2. Get all items containing only Vitamin C.
    3. Get all items containing Vitamin A.
    4. Group items based on the Vitamins that they contain in the following format:
        {
            "Vitamin C": ["Orange", "Mango"],
            "Vitamin K": ["Mango"],
        }
        
        and so on for all items and all Vitamins.
    5. Sort items based on number of Vitamins they contain.

    NOTE: Do not change the name of this file

*/
function availableOnly(data) {
    let result = data.filter((record) => record.available)
    console.log(result)

    return result
}

function onlyVitaminC(data) {
    let result = data.filter((record) => record.contains == "Vitamin C")
    console.log(result)

    return result
}
function containsVitaminA(data) {
    let result = data.filter((record) => record.contains.includes("Vitamin A"))
    console.log(result)

    return result
}

function groupBasedOnVitamins(data) {
    let result = data.reduce((accumulator, record) => {
        let vitamins = record.contains.split(",")
        vitamins.map((vitamin) => {
            vitamin = vitamin.trim()
            if (Object.keys(accumulator).includes(vitamin)) {
                accumulator[vitamin].push(record.name)
            } else {
                accumulator[vitamin] = new Array()
                accumulator[vitamin].push(record.name)
            }
        })
        return accumulator
    }, {})
    console.log(result)

    return result
}

function sortBasedOnNumberOfVitamins(data) {
    let result = data.map((record) => {
        record["Number_of_vitamins"] = record.contains.split(",").length

        return record
    })
    console.log(result)
    result = result.sort((record, recordOne) => {
        let num = record.Number_of_vitamins
        let numOne = recordOne.Number_of_vitamins

        return numOne - num
    })

    console.log(result)

    return result
}
availableOnly(items)
onlyVitaminC(items)
containsVitaminA(items)
groupBasedOnVitamins(items)
sortBasedOnNumberOfVitamins(items)
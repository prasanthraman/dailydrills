/*
 * Use asynchronous callbacks ONLY wherever possible.
 * Error first callbacks must be used always.
 * Each question's output has to be stored in a json file.
 * Each output file has to be separate.

 * Ensure that error handling is well tested.
 * After one question is solved, only then must the next one be executed. 
 * If there is an error at any point, the subsequent solutions must not get executed.
   
 * Store the given data into data.json
 * Read the data from data.json
 * Perfom the following operations.

    1. Retrieve data for ids : [2, 13, 23].
    2. Group data based on companies.
        { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
    3. Get all data for company Powerpuff Brigade
    4. Remove entry with id 2.
    5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
    6. Swap position of companies with id 93 and id 92.
    7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

    NOTE: Do not change the name of this file

*/

// {
//     "employees": [
//         {
//             "id": 23,
//             "name": "Daphny",
//             "company": "Scooby Doo"
//         },
//         {
//             "id": 73,
//             "name": "Buttercup",
//             "company": "Powerpuff Brigade"
//         },
//         {
//             "id": 93,
//             "name": "Blossom",
//             "company": "Powerpuff Brigade"
//         },
//         {
//             "id": 13,
//             "name": "Fred",
//             "company": "Scooby Doo"
//         },
//         {
//             "id": 89,
//             "name": "Welma",
//             "company": "Scooby Doo"
//         },
//         {
//             "id": 92,
//             "name": "Charles Xavier",
//             "company": "X-Men"
//         },
//         {
//             "id": 94,
//             "name": "Bubbles",
//             "company": "Powerpuff Brigade"
//         },
//         {
//             "id": 2,
//             "name": "Xyclops",
//             "company": "X-Men"
//         }
//     ]
// }

const fs=require("fs")
const path=require("path")

function employeesCallback(callback){
    fs.readFile(path.join(__dirname,`data.json`),{encoding:'utf-8'},(err,data)=>{
        if(err){
            callback(err)
        }else{
            data=JSON.parse(data)
            data=data.employees
            let requiredIds=[2, 13, 23]
            data=data.filter((record)=>{
                return requiredIds.includes(record.id)
            })
            data=JSON.stringify(data)
            fs.writeFile(path.join(__dirname,`requiredEmployees.json`),data,(err)=>{
                if(err){
                    callback(err)
                }else{
                    callback(null,"Write Success for Problem 1")
                }
            })
            
        }
    })

}
function groupBasedOnCompanies(callback){
    fs.readFile(path.join(__dirname,`data.json`),{encoding:'utf-8'},(err,data)=>{
        if(err){
            callback(err)
        }else{
            data=JSON.parse(data)
            data=data.employees
            data=data.reduce((acc,record)=>{
                if(acc.hasOwnProperty(record.company)){
                    acc[record.company].push(record)
                }else{
                    acc[record.company]=new Array()
                    acc[record.company].push(record)
                }
                return acc
            },{})
            data=JSON.stringify(data)
            fs.writeFile(path.join(__dirname,`groupBasedOnCompanies.json`),data,(err)=>{
                if(err){
                    callback(err)
                }else{
                    callback(null,"Write Success for Problem 2")
                }
            })
            
        }
    })

}

function getDataPowerPuff(callback){
    fs.readFile(path.join(__dirname,`data.json`),{encoding:'utf-8'},(err,data)=>{
        if(err){
            callback(err)
        }else{
            data=JSON.parse(data)
            data=data.employees
            data=data.filter((record)=>{
                return (record.company=="Powerpuff Brigade")
            })
            
            data=JSON.stringify(data)
            fs.writeFile(path.join(__dirname,`powerPuff.json`),data,(err)=>{
                if(err){
                    callback(err)
                }else{
                    callback(null,"Write Success for Problem 3")
                }
            })
            
        }
    })

}
function removeId(callback){
    fs.readFile(path.join(__dirname,`data.json`),{encoding:'utf-8'},(err,data)=>{
        if(err){
            callback(err)
        }else{
            data=JSON.parse(data)
            data=data.employees
            data=data.filter((record)=>{
                return (record.id!==2)
            })
            
            data=JSON.stringify(data)
            fs.writeFile(path.join(__dirname,`removedIds.json`),data,(err)=>{
                if(err){
                    callback(err)
                }else{
                    callback(null,"Write Success for Problem 4")
                }
            })
            
        }
    })

}

function sortBasedOnCompany(callback){
    fs.readFile(path.join(__dirname,`data.json`),{encoding:'utf-8'},(err,data)=>{
        if(err){
            callback(err)
        }else{
            data=JSON.parse(data)
            data=data.employees
            data=data.sort((recordA,recordB)=>{
                if(recordA.company>recordB.company){
                    return 1
                }else if(recordB.company>recordA.company){
                    return -1
                }else {
                    return recordA.id-recordB.id
                }
            })
            data=JSON.stringify(data)
            fs.writeFile(path.join(__dirname,`sortedData.json`),data,(err)=>{
                if(err){
                    callback(err)
                }else{
                    callback(null,"Write Success for Problem 5")
                }
            })
            
        }
    })

}
function swapIds(callback){
    fs.readFile(path.join(__dirname,`data.json`),{encoding:'utf-8'},(err,data)=>{
        if(err){
            callback(err)
        }else{
            let tempIndex=null
            let tempValue=null
            data=JSON.parse(data)
            data=data.employees
            data=data.map((record,index)=>{
                if(record.id==92 || record.id==93){
                    if(tempIndex==null){
                        tempIndex=index
                    }else{
                        tempValue=record
                        return data[tempIndex]
                    }
                }
                return record
            })
            data[tempIndex]=tempValue
            data=JSON.stringify(data)
            fs.writeFile(path.join(__dirname,`swap.json`),data,(err)=>{
                if(err){
                    callback(err)
                }else{
                    callback(null,"Write Success for Problem 6")
                }
            })
            
        }
    })


}
employeesCallback((err,data)=>{
    if(err){
        console.log(err)
    }else{
        console.log(data)
        groupBasedOnCompanies((err,data)=>{
            if(err){
                console.log(err)
            }else{
                console.log(data)
                getDataPowerPuff((err,data)=>{
                    if(err){
                        console.log(err)
                    }else{
                        console.log(data)
                        removeId((err,data)=>{
                            if(err){
                                console.log(err)
                            }else{
                                console.log(data)
                                sortBasedOnCompany((err,data)=>{
                                    if(err){
                                        console.log(err)
                                    }else{
                                        console.log(data)
                                        swapIds((err,data)=>{
                                            if(err){
                                                console.log(err)
                                            }else{
                                                console.log(data)
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }
                })
            }
        })
    }
})
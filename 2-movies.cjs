const favouritesMovies = {
    "Matrix": {
        imdbRating: 8.3,
        actors: ["Keanu Reeves", "Carrie-Anniee"],
        oscarNominations: 2,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$680M"
    },
    "FightClub": {
        imdbRating: 8.8,
        actors: ["Edward Norton", "Brad Pitt"],
        oscarNominations: 6,
        genre: ["thriller", "drama"],
        totalEarnings: "$350M"
    },
    "Inception": {
        imdbRating: 8.3,
        actors: ["Tom Hardy", "Leonardo Dicaprio"],
        oscarNominations: 12,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$870M"
    },
    "The Dark Knight": {
        imdbRating: 8.9,
        actors: ["Christian Bale", "Heath Ledger"],
        oscarNominations: 12,
        genre: ["thriller"],
        totalEarnings: "$744M"
    },
    "Pulp Fiction": {
        imdbRating: 8.3,
        actors: ["Sameul L. Jackson", "Bruce Willis"],
        oscarNominations: 7,
        genre: ["drama", "crime"],
        totalEarnings: "$455M"
    },
    "Titanic": {
        imdbRating: 8.3,
        actors: ["Leonardo Dicaprio", "Kate Winslet"],
        oscarNominations: 13,
        genre: ["drama"],
        totalEarnings: "$800M"
    }
}


/*
    NOTE: For all questions, the returned data must contain all the movie information including its name.

    Q1. Find all the movies with total earnings more than $500M. 
    Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
    Q.3 Find all movies of the actor "Leonardo Dicaprio".
    Q.4 Sort movies (based on IMDB rating)
        if IMDB ratings are same, compare totalEarning as the secondary metric.
    Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
        drama > sci-fi > adventure > thriller > crime

    NOTE: Do not change the name of this file
*/
function earnedAbove(data) {
    let result = Object.entries(data)
    result = result.filter(([, value]) => {
        return parseFloat(value.totalEarnings.slice(1, -1)) > 500
    })

    result = Object.fromEntries(result)
    //console.log(result)

    return result
}

function moreThanThreeOscarNominations(data) {
    let result = earnedAbove(data)
    result = Object.entries(result)

    result = result.filter(([, value]) => {
        return value.oscarNominations > 3
    })
    result = Object.fromEntries(result)
    //console.log(result)

    return result
}

function moviesOfLeonardo(data) {
    let result = Object.entries(data)
    result = result.filter(([, value]) => {
        return value.actors.includes("Leonardo Dicaprio")
    })
    result = Object.fromEntries(result)
    console.log(result)

    return result
}

function sortMovies(data) {
    let result = Object.entries(data)
    result = result.sort(([, valueA], [, valueB]) => {
        let ratingA = valueA.imdbRating
        let ratingB = valueB.imdbRating
        let earningA = parseFloat(valueA.totalEarnings.slice(1, -1))
        let earningB = parseFloat(valueB.totalEarnings.slice(1, -1))

        if (ratingA === ratingB) {
            return earningA - earningB
        }

        return ratingB - ratingA
    })
    result = Object.fromEntries(result)
    console.log(result)

    return result
}

function groupMovies(favouritesMovies) {
    let result = Object.entries(favouritesMovies)
    result = result.reduce((acc, [key, value]) => {
        let genre
        if (value.genre.includes("drama")) {
            genre = "drama"
        } else if (value.genre.includes("sci-fi")) {
            genre = "sci-fi"
        } else if (value.genre.includes("adventure")) {
            genre = "adventure"
        } else if (value.genre.includes("thriller")) {
            genre = "thriller"
        } else if (value.genre.includes("crime")) {
            genre = "crime"
        } else {
            genre = "others"
        }
        if (acc.hasOwnProperty(genre)) {
            acc[genre].push(key)
        } else {
            acc[genre] = new Array()
            acc[genre].push(key)
        }

        return acc
    }, {})

    console.log(result)
    return result
}

earnedAbove(favouritesMovies)
moreThanThreeOscarNominations(favouritesMovies)
moviesOfLeonardo(favouritesMovies)
sortMovies(favouritesMovies)
groupMovies(favouritesMovies)

/*
NOTE: Do not change the name of this file

* Ensure that error handling is well tested.
* If there is an error at any point, the subsequent solutions must not get executed.
* Solutions without error handling will get rejected and you will be marked as having not completed this drill.

* Usage of async and await is not allowed.

Users API url: https://jsonplaceholder.typicode.com/users
Todos API url: https://jsonplaceholder.typicode.com/todos

Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=2302913
Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=2321392

Using promises and the `fetch` library, do the following. 

1. Fetch all the users
2. Fetch all the todos
3. Use the promise chain and fetch the users first and then the todos.
4. Use the promise chain and fetch the users first and then all the details for each user.
5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo

NOTE: If you need to install `node-fetch` or a similar libary, let your mentor know before doing so along with the reason why. No other exteral libraries are allowed to be used.

Usage of the path libary is recommended


*/
function getAllUsers() {
    return new Promise((resolve, reject) => {
        fetch('https://jsonplaceholder.typicode.com/users')
            .then((response) => {
                return response.json()
            })
            .then((data) => {
                resolve(data)
            })
            .catch((err) => {
                reject(err)
            })

    })

}

function getAllTodos() {
    return new Promise((resolve, reject) => {
        fetch('https://jsonplaceholder.typicode.com/todos')
            .then((response) => {
                return response.json()
            })
            .then((todos) => {
                resolve(todos)
            })
            .catch((err) => {
                console.log(err)
                reject(err)
            })

    })

}
function getUsersThenTodos() {
    return new Promise((resolve, reject) => {
        let usersAndTodos = []
        getAllUsers()
            .then((users) => {
                usersAndTodos.push(users)
                return getAllTodos()
            })
            .then((todos) => {
                usersAndTodos.push(todos)
                resolve(usersAndTodos)
            })
            .catch(err => {
                reject(err)
            })

    })
}

function getUsersThenDetails() {
    return new Promise((resolve, reject) => {
        let usersList
        let details
        getAllUsers()
            .then((users) => {
                usersList=users
                details=users.map((record)=>{
                    return getSpecificUser(record.id)
                })
                return details
            })
            .then((detailsPromises)=>{
                return Promise.all(detailsPromises)
            })
            .then((detailsPromises)=>{
                resolve([usersList,detailsPromises])
            })
            .catch((err)=>{
                reject(err)
            })
    })
}

function getFirstTodoandUserDetails(){
    return new Promise((resolve,reject)=>{
        let firstTodo
        getAllTodos()
            .then((todos)=>{
                firstTodo=todos[0]
                return getSpecificUser(firstTodo.userId)
            })
            .then((user)=>{
                resolve([firstTodo,user])
            })
            .catch((err)=>{
                reject(err)
            })
    })
}

function getSpecificUser(id) {
    return new Promise((resolve, reject) => {
        fetch(`https://jsonplaceholder.typicode.com/users?id=${id}`)
            .then((response) => {
                return response.json()
            })
            .then((user) => {
                resolve(user)
            })
            .catch((err) => {
                console.log(err)
                reject(err)
            })

    })
}

getAllUsers()
    .then((users) => {
        console.log(users)
    })
    .then(() => {
        return getAllTodos()
    })
    .then((todos) => {
        console.log(todos)
    })
    .then(() => {
        return getUsersThenTodos()
    })
    .then(([users, todos]) => {
        console.log(users, todos)
    })
    .then(()=>{
        return getUsersThenDetails()
    })
    .then(([users,details])=>{
        console.log(users,details)
    })
    .then(()=>{
        return getFirstTodoandUserDetails()
    })
    .then(([firstTodo,user])=>{
        console.log(firstTodo,user)
    })
    .catch((err) => {
        console.log(err)
    })
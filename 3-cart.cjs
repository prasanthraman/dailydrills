const products = [{
    shampoo: {
        price: "$50",
        quantity: 4
    },
    "Hair-oil": {
        price: "$40",
        quantity: 2,
        sealed: true
    },
    comb: {
        price: "$12",
        quantity: 1
    },
    utensils: [
        {
            spoons: { quantity: 2, price: "$8" }
        }, {
            glasses: { quantity: 1, price: "$70", type: "fragile" }
        }, {
            cooker: { quantity: 4, price: "$900" }
        }
    ],
    watch: {
        price: "$800",
        quantity: 1,
        type: "fragile"
    }
}]


/*

Q1. Find all the items with price more than $65.
Q2. Find all the items where quantity ordered is more than 1.
Q.3 Get all items which are mentioned as fragile.
Q.4 Find the least and the most expensive item for a single quantity.
Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)

NOTE: Do not change the name of this file

*/

function costlyItems(data) {
    data = data[0]
    let utensils = data.utensils
    utensils = utensils.reduce((acc, record) => {
        acc.push([Object.keys(record), Object.values(record)])
        data[Object.keys(record)[0]] = Object.values(record)[0]
        return acc
    }, [])
    //console.log(data)
    data = Object.entries(data)
    data = data.reduce((acc, [key, value]) => {
        let price
        if (key == 'utensils') {
            return acc
        }
        price = Number(value.price.slice(1))
        if (price > 65) {
            acc.push([key, value])
        }


        return acc
    }, [])

    data = Object.fromEntries(data)
    console.log(data)
    return data
}

function orderedMore(data) {
    data = data[0]
    let utensils = data.utensils
    utensils = utensils.reduce((acc, record) => {
        acc.push([Object.keys(record), Object.values(record)])
        data[Object.keys(record)[0]] = Object.values(record)[0]
        return acc
    }, [])
    // aconsole.log(data)
    data = Object.entries(data)

    data = data.reduce((acc, [key, value]) => {
        let orders
        if (key == 'utensils') {
            return acc
        }
        orders = value.quantity
        if (orders > 1) {
            acc.push([key, value])
        }
        return acc


    }, [])

    data = Object.fromEntries(data)
    console.log(data)
    return data

}
function fragileProducts(data) {
    data = data[0]
    let utensils = data.utensils
    utensils = utensils.reduce((acc, record) => {
        acc.push([Object.keys(record), Object.values(record)])
        data[Object.keys(record)[0]] = Object.values(record)[0]
        return acc
    }, [])
    data = Object.entries(data)

    data = data.reduce((acc, [key, value]) => {
        let type
        if (key == 'utensils') {
            return acc
        }
        type = value.type
        if (type == 'fragile') {
            acc.push([key, value])
        }


        return acc
    }, [])

    data = Object.fromEntries(data)
    console.log(data)
    return data

}

function leastMostExpensive(data) {
    data = data[0]
    let utensils = data.utensils
    utensils = utensils.reduce((acc, record) => {
        acc.push([Object.keys(record), Object.values(record)])
        data[Object.keys(record)[0]] = Object.values(record)[0]
        return acc
    }, [])
    data = Object.entries(data)

    data = data.reduce((acc, [key, value]) => {
        let orders
        if (key == 'utensils') {
            return acc
        }
        orders = value.quantity
        if (orders == 1) {

            acc.push([key, value])
        }

        return acc

    }, [])

        .sort(([, valueA], [, valueB]) => {
            let priceA = Number(valueA.price.slice(1))
            let priceB = Number(valueB.price.slice(1))

            return priceB - priceA
        })
    data = [data.at(0), data.at(-1)]
    data = Object.fromEntries(data)
    console.log(data)
    return data

}

costlyItems(products)
orderedMore(products)
fragileProducts(products)
leastMostExpensive(products)